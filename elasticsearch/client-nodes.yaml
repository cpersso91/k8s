
apiVersion: apps/v1
kind: Deployment
metadata:
  name: es-client
  namespace: elasticsearch
  labels:
    component: elasticsearch
    role: client
spec:
  replicas: 1
  selector:
    matchLabels:
      role: client
      component: elasticsearch
  template:
    metadata:
      labels:
        component: elasticsearch
        role: client
    spec:
      affinity:
        podAntiAffinity:
          preferredDuringSchedulingIgnoredDuringExecution:
            - weight: 100
              podAffinityTerm:
                labelSelector:
                  matchExpressions:
                    - key: role
                      operator: In
                      values:
                        - client
                topologyKey: kubernetes.io/hostname
      initContainers:
        - name: init-sysctl
          image: busybox:1.27.2
          command:
            - sysctl
            - -w
            - vm.max_map_count=262144
          securityContext:
            privileged: true
        - name: init-es
          image: byrnedo/alpine-curl
          command:
            - sh
            - /tmp/init/init.sh
          volumeMounts:
            - name: init-script-volume
              mountPath: /tmp/init
      containers:
        - name: es-client
          image: quay.io/clrxbl/docker-elasticsearch-kubernetes:6.5.2
          env:
            - name: NAMESPACE
              valueFrom:
                fieldRef:
                  fieldPath: metadata.namespace
            - name: NODE_NAME
              valueFrom:
                fieldRef:
                  fieldPath: metadata.name
            - name: CLUSTER_NAME
              value: my-es
            - name: NODE_MASTER
              value: "false"
            - name: NODE_DATA
              value: "false"
            - name: HTTP_ENABLE
              value: "true"
            - name: ES_JAVA_OPTS
              value: -Xms8g -Xmx8g
            - name: NETWORK_HOST
              value: 0.0.0.0
            - name: "ES_PLUGINS_INSTALL"
              value: "repository-s3"
            - name: PROCESSORS
              valueFrom:
                resourceFieldRef:
                  resource: limits.cpu
          resources:
            requests:
              cpu: 0.5
              memory: 8Gi
            limits:
              cpu: 1
              memory: 10Gi
          ports:
            - containerPort: 9200
              name: http
            - containerPort: 9300
              name: transport
          livenessProbe:
            tcpSocket:
              port: transport
            initialDelaySeconds: 120
            periodSeconds: 10
          readinessProbe:
            httpGet:
              path: /_cluster/health
              port: http
            initialDelaySeconds: 40
            timeoutSeconds: 5
          volumeMounts:
            - name: storage
              mountPath: /data
      volumes:
        - emptyDir:
            medium: ""
          name: storage
        - name: init-script-volume
          configMap:
            name: init-script

---
apiVersion: v1
kind: Service
metadata:
  name: elasticsearch
  namespace: elasticsearch
  labels:
    component: elasticsearch
    role: client
spec:
  selector:
    component: elasticsearch
    role: client
  ports:
    - name: http
      port: 9200


---
apiVersion: v1
kind: ConfigMap
metadata:
  name: init-script
  namespace: elasticsearch
data:
  init.sh: |-
    curl -X PUT http://elasticsearch-data-hot:9200/_template/default -H 'Content-Type: application/json' -d'{
      "index_patterns" : "*",
      "version" : 60001,
      "settings" : {
        "index": {
          "refresh_interval" : "5s",
          "routing.allocation.require.box_type": "hot",
          "default_pipeline":"timestamp_rename"
        },
        "number_of_shards": 1,
        "number_of_replicas": 1,
        "codec": "best_compression"
      }
    }
    '

    curl -X PUT http://elasticsearch-data-hot:9200/_ingest/pipeline/timestamp_rename -H 'Content-Type: application/json' -d'{
      "description" : "describe pipeline",
      "processors" : [{
        "rename": {
          "field": "timestamp",
          "target_field": "@timestamp",
          "ignore_missing": true
        }
      }]
    }
    '

    curl -X PUT http://elasticsearch-data-hot:9200/_snapshot/s3-es-snapshots  -H 'Content-Type: application/json' -d'{
      "type": "s3",
      "settings": {
        "bucket": "s3-es-snapshot-repository.volvocars.biz"
      }
    }
    '
