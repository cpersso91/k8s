#!/usr/bin/env bash

SPOT_PRICE_MARGIN="0.01"
KUBERNETES_VERSION="1.11.6"
KOPS_CMD=kops

source "$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )/../commons/commons.sh"

cluster:s3_bucket(){
  local bucket_name="${1}"
  local region=${2}
  exists=$(aws s3api list-buckets \
      --query "Buckets[].Name" \
      | jq ". | index(\"${bucket_name}\") != null"
  )
  if [[ "${exists}" == "false" ]]; then
    printf "${RED}Creating S3 bucket ${bucket_name}${NC}\n"
    aws s3api create-bucket \
        --bucket ${bucket_name} \
        --region ${region} \
        --create-bucket-configuration LocationConstraint=${region}

    aws s3api put-bucket-versioning \
      --bucket ${bucket_name} \
      --versioning-configuration Status=Enabled \
      --region ${region}
  else
    printf "Using existing S3 bucket ${GREEN}${bucket_name}${NC}\n"
  fi
}

cluster:create_state(){
  local cluster_name="${1}"
  local dns_zone=${2}
  local region=${3}
  local master_zones=${4}
  local min_nodes=${5}
  local max_nodes=${6}
  local node_instance_type=${7}
  local master_instance_type=${8}
  local cloud_labels=${9:-""}
  local ssh_key=${10:-"${HOME}/.ssh/id_rsa.pub"}

  if [[ ! -f ${ssh_key} ]]; then
    echo "${ssh_key} not found"
    exit 1
  fi

  local TF_OUT="./clusters/${cluster_name}"
  local ZONES="${region}a,${region}b,${region}c"
  echo "Creating initial state, might take a few seconds"

  ${KOPS_CMD} create cluster \
      --kubernetes-version=${KUBERNETES_VERSION} \
      --name=${cluster_name} \
      --node-count ${min_nodes} \
      --zones ${ZONES} \
      --master-zones "${master_zones}" \
      --dns-zone ${dns_zone} \
      --node-size ${node_instance_type} \
      --master-size ${master_instance_type} \
      --topology public \
      --ssh-public-key ${ssh_key} \
      --networking calico \
      --encrypt-etcd-storage \
      --authorization=AlwaysAllow \
      --out=${TF_OUT} \
      --target=direct \
      --cloud=aws \
      --cloud-labels ${cloud_labels}
      #  &>/dev/null

 }

cluster:create_resources() {
  local cluster_name="${1}"
  printf "Creating resources, this might take a few minutes\n"
  ${KOPS_CMD} update cluster ${cluster_name} --yes #&>/dev/null
}

cluster:convert_to_spot_instances() {
  local cluster_name="${1}"
  local master_zones=${2}
  local region=${3}

  cluster:spot_instances ${cluster_name} "nodes" ${region}
  for master_zone in ${master_zones//,/ }; do
    cluster:spot_instances ${cluster_name} "master-${master_zone}" ${region}
  done
}

cluster:spot_instances() {
  local cluster_name="${1}"
  local group="${2}"
  local region=${3}
  local instance_type=$(${KOPS_CMD} get ig ${group} --name ${cluster_name} -o yaml | grep -oP "machineType: \K(.*)")

  local spot_price=$(cluster:spot_price_for_instance ${instance_type} ${region})
  local max_price=$(echo "$SPOT_PRICE_MARGIN $spot_price" | awk '{ printf "%f", $1 + $2 }')

  ${KOPS_CMD} get ig ${group} --name ${cluster_name} -o yaml \
    | sed -e "/maxSize/i\  maxPrice: \"${max_price}\"" \
    | ${KOPS_CMD} replace ig ${group} --name ${cluster_name} -f -

  printf "Changed instancegroup ${BLUE}${group}${NC} to use spot instances of type ${BLUE}${instance_type}${NC} with price ${BLUE}${max_price}${NC}\n"
}

cluster:spot_price_for_instance() {
  local instance_type=${1}
  local region=${2}
  aws ec2 describe-spot-price-history \
    --region ${region} \
    --instance-types ${instance_type} \
    --product-description "Linux/UNIX" "Linux/UNIX (Amazon VPC)" \
    --start-time "$(date --date '2 months ago' \
    --iso-8601)" \
    | jq -r '.SpotPriceHistory[].SpotPrice' \
    | sort -ur \
    | head -1 \
    | awk '{ sum += $1; n++ } END { if (n > 0) print sum / n; }'
}

cluster:extra_iam_policies() {
  local cluster_name=${1}
  local node_policies=$(cluster:policies node)
  local master_policies=$(cluster:policies master)

  ${KOPS_CMD} get cluster ${cluster_name} -o yaml \
    | sed -e "/spec:/a\  additionalPolicies:\n    ${node_policies}\n    ${master_policies}" \
    | ${KOPS_CMD} replace cluster ${cluster_name} -f -
}

cluster:policies() {
  local name=${1}
  local iam_path="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )/iam-policies"

  if [[ $(ls ${iam_path}/${name}/*.json 2>/dev/null)  ]]; then
    local policy_string=$(jq --compact-output -r --slurp '{ Statement: map(.Statement[]) } | .Statement' ${iam_path}/${name}/*.json | awk '{ gsub(/"/,"\\\"") } 1')
    echo "${name}: '${policy_string}'"
  else
    echo ""
  fi
}

cluster:set_autoscaling_size() {
  local cluster_name="${1}"
  local group="${2}"
  local min_size=${3}
  local max_size=${4}

  ${KOPS_CMD} get ig ${group} --name ${cluster_name} -o yaml \
    | sed -e "s/minSize:.*/minSize: ${min_size}/" \
      -e "s/maxSize:.*/maxSize: ${max_size}/" \
      -e "/nodeLabels/i\  cloudLabels:\n    k8s.io/cluster-autoscaler/enabled: \"true\"\n    k8s.io/cluster-autoscaler/${cluster_name}: \"true\"" \
    | ${KOPS_CMD} replace ig ${group} --name ${cluster_name} -f -

  printf "Changed instancegroup ${BLUE}${group}${NC} min: ${BLUE}${min_size}${NC} max: ${BLUE}${max_size}${NC}\n"

}

cluster:instance_group_config() {
  local cluster_name="${1}"
  local dns=$(echo ${cluster_name} | cut -d. -f2-)
  local group="${2}"
  local instance_type=${3}
  local min_size=${4}
  local max_size=${5}
  cluster:bucket_name ${dns}
  tmp_file=/tmp/.${cluster_name}-$(date "+%Y%m%d%H%M%S")

  ${KOPS_CMD} get ig ${group} --name ${cluster_name} -o yaml > ${tmp_file}

  if [[ ${min_size} != "-" ]]; then
      sed -i -e "s/minSize:.*/minSize: ${min_size}/" ${tmp_file}
  fi
  if [[ ${max_size} != "-" ]]; then
      sed -i -e "s/maxSize:.*/maxSize: ${max_size}/" ${tmp_file}
  fi
  if [[ ${instance_type} != "-" ]]; then
      sed -i -e "s/machineType:.*/machineType: ${instance_type}/" ${tmp_file}
  fi

  if diff -q <(kops get ig ${group} --name ${cluster_name} -o yaml) ${tmp_file} &> /dev/null; then
    echo "No changes detected"
  else
    ${KOPS_CMD} replace ig ${group} --name ${cluster_name} -f ${tmp_file}
    cluster:create_resources ${cluster_name}
    ${KOPS_CMD} rolling-update cluster ${cluster_name} --yes
  fi

}

cluster:region_master_zones() {
  local master_zones=${1}
  local region=${2}
  local result=""
  for zone in ${master_zones//,/ }; do
    result="${region}${zone},${result}"
  done
  echo ${result} | sed 's/.$//'
}

cluster:bucket_name() {
  local dns="${1}"
  local account_id="${2}"
  local bucket_name="${account_id}-${dns}-kops-storage"
  echo "${bucket_name}"
}

cluster:create() {
  local name="${1}"
  local dns="${2}"
  local region=${4}
  local master_zones=$(cluster:region_master_zones ${3} ${region})
  local min_nodes=${5}
  local max_nodes=${6}
  local node_instance_type=${7}
  local master_instance_type=${8}
  local cloud_labels=${9:-""}

  local cluster_name="${name}.${dns}"
  printf "Creating cluster ${BLUE}${cluster_name}${NC}\n"


  local account_id=$(commons:aws_account_id)
  # TODO Fix this ugliness
  local bucket_name=$(cluster:bucket_name "${dns}" "${account_id}")
  export KOPS_STATE_STORE="s3://${bucket_name}"

  if ${KOPS_CMD} get ${cluster_name} &> /dev/null; then
      printf "Cluster ${GREEN}${cluster_name}${NC} already exist."
      return
  fi
  cluster:s3_bucket ${bucket_name} ${region}
  cluster:create_state ${cluster_name} ${dns} ${region} ${master_zones} ${min_nodes} ${max_nodes} ${node_instance_type} ${master_instance_type} ${cloud_labels}
  cluster:convert_to_spot_instances ${cluster_name} ${master_zones} ${region}
  cluster:set_autoscaling_size ${cluster_name} "nodes" ${min_nodes} ${max_nodes}
  cluster:extra_iam_policies ${cluster_name}

  cluster:create_resources ${cluster_name}
  # WAIT!!!
  printf "Waiting for cluster ${GREEN}${cluster_name}${NC} to become ready, this might take a few minutes\n"
  local start=$(date "+%s")
  while true; do
    local current=$(date "+%s")
    local waited=$(echo "${current} - ${start}" | bc)
    printf "\rWaited $waited seconds"
    ${KOPS_CMD} validate cluster ${cluster_name}  &> /dev/null && break
  done
  printf "\n${GREEN}Done creating cluster${NC}\n\n"
}


