#!/usr/bin/env bash
set -euo pipefail
source commons.sh

postgres:local() {
  if [[ $(helm list | grep postgres) ]]
  then
    printf "${GREEN}Postgres already installed.${NC}\n"
  else
    printf "${RED}Installing Postgres${NC}\n"

    helm install --name=postgres \
    --set persistence.enabled=false,postgresPassword=postgres,service.type=LoadBalancer \
    stable/postgresql
  fi
}

postgres:main() {
  shopt -s nullglob
  local SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
  for FILE in ${SCRIPT_DIR}/{.,${ENVIRONMENT}}/*.yaml; do
    ${KUBECTL_CMD} apply --record=false -f ${FILE}
  done

  if [[ "${ENVIRONMENT}" == "local" ]]; then
    postgres:local
  fi
}
