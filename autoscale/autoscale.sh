#!/usr/bin/env bash

autoscale:create() {
  local cluster_name="${1}"
  local region="${2}"

  cat "$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )/cluster-autoscaler.yaml" \
    | sed -e "s/\$\$CLUSTER_NAME\$\\$/${cluster_name}/" -e "s/\$\$REGION\$\\$/${region}/" \
    | ${KUBECTL_CMD} apply -f -

}
