#!/usr/bin/env bash
set -euo pipefail

rabbitmq:secrets() {
  local SECRET_NAME="rabbitmq"

  if [[ "${ENVIRONMENT}" == "local" ]]
  then
    local RABBITMQ_PASSWORD="password"
  else
    echo "Unsupported"; exit 1
  fi

  ${KUBECTL_CMD} delete secret shared-rabbitmq &> /dev/null || true
  ${KUBECTL_CMD} create secret generic \
    shared-rabbitmq \
    --from-literal=RABBITMQ_PASSWORD="${RABBITMQ_PASSWORD}"
}

rabbitmq:main() {
  rabbitmq:secrets
  shopt -s nullglob
  local SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

  for FILE in ${SCRIPT_DIR}/{.,${ENVIRONMENT}}/*.yaml; do
    ${KUBECTL_CMD} apply --record=false -f ${FILE}
  done

}

